import { Router } from "express";
import {
  createCategory,
  getCategories,
  deleteCategory,
  updateCategory,
} from "../controllers/category.controller";

const router = Router();
router.get("/category", getCategories);
router.post("/category", createCategory);
router.delete("/category/:id", deleteCategory);
router.put("/category/:id", updateCategory);

export default router;
