import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  JoinColumn,
  OneToOne,
  ManyToOne,
} from "typeorm";
import { Category } from "./category";

@Entity()
export class Product {
  @PrimaryGeneratedColumn()
  id: number;
  @Column()
  name: string;
  @Column()
  url_image: string;
  @Column()
  price: number;
  @Column()
  discount: number;

  @ManyToOne(() => Category, (category) => category.products)
  category: Category;
}
