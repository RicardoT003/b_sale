import { Request, Response } from "express";
import { getRepository } from "typeorm";
import { Product } from "../entity/product";

export const getProducts = async (
  req: Request,
  res: Response
): Promise<Response> => {
  try {
    const products = await getRepository(Product).find();
    return res.json(products);
  } catch (error) {
    return res.json({ msg: error });
  }
};

export const createProduct = async (
  req: Request,
  res: Response
): Promise<Response> => {
  const newProduct = await getRepository(Product).create(req.body);
  const results = await getRepository(Product).save(newProduct);
  return res.json(results);
};

export const deleteProduct = async (
  req: Request,
  res: Response
): Promise<Response> => {
  console.log("xd", req.params.id);

  const id = req.params.id;
  const response = await getRepository(Product).delete(id);
  return res.json(response);
};

export const updateProduct = async (
  req: Request,
  res: Response
): Promise<Response> => {
  const id = req.params.id;
  const user = await getRepository(Product).findOne(id);
  if (user) {
    getRepository(Product).merge(user, req.body); // Reemplaza algunos valore(valores nuevos)
    const result = await getRepository(Product).save(user);
    return res.json(result);
  }
  return res.json({ message: "error" });
};
