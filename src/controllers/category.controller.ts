import { Request, Response } from "express";
import { getRepository } from "typeorm";
import { Category } from "../entity/category";

export const getCategories = async (
  req: Request,
  res: Response
): Promise<Response> => {
  try {
    const categories = await getRepository(Category).find();
    return res.json(categories);
  } catch (error) {
    return res.json({ msg: error });
  }
};

export const createCategory = async (
  req: Request,
  res: Response
): Promise<Response> => {
  const newCategory = await getRepository(Category).create(req.body);
  const results = await getRepository(Category).save(newCategory);
  return res.json(results);
};

export const deleteCategory = async (
  req: Request,
  res: Response
): Promise<Response> => {
  console.log("xd", req.params.id);

  const id = req.params.id;
  const response = await getRepository(Category).delete(id);
  return res.json(response);
};

export const updateCategory = async (
  req: Request,
  res: Response
): Promise<Response> => {
  const id = req.params.id;
  const category = await getRepository(Category).findOne(id);
  if (category) {
    getRepository(Category).merge(category, req.body); // Reemplaza algunos valore(valores nuevos)
    const result = await getRepository(Category).save(category);
    return res.json(result);
  }
  return res.json({ message: "error" });
};
