import express from "express";
import morgan from "morgan";
import cors from "cors";
import productRoutes from "./routes/product.route";
import categoryRoutes from "./routes/category.route";

import "reflect-metadata";
import { createConnection } from "typeorm";
import helmet from "helmet";
const app = express();

createConnection();

//Middlewares
app.use(cors());
app.use(morgan("dev"));
app.use(express.json());
app.use(helmet());

// Routes
app.use(productRoutes);
app.use(categoryRoutes);

app.listen(4000);
console.log(4000);
