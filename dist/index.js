"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = __importDefault(require("express"));
var morgan_1 = __importDefault(require("morgan"));
var cors_1 = __importDefault(require("cors"));
var product_route_1 = __importDefault(require("./routes/product.route"));
var category_route_1 = __importDefault(require("./routes/category.route"));
require("reflect-metadata");
var typeorm_1 = require("typeorm");
var helmet_1 = __importDefault(require("helmet"));
var app = express_1.default();
typeorm_1.createConnection();
//Middlewares
app.use(cors_1.default());
app.use(morgan_1.default("dev"));
app.use(express_1.default.json());
app.use(helmet_1.default());
// Routes
app.use(product_route_1.default);
app.use(category_route_1.default);
app.listen(4000);
console.log(4000);
